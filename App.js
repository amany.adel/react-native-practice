import { createStackNavigator, createAppContainer } from 'react-navigation';
import HomeScreen from './src/screens/HomeScreen';
import FirstComponent from './src/screens/FirstScreen';
import arr from './src/screens/ListScreen'
import ImgDetails from './src/components/imgDetails';
import CounterScreen from './src/screens/CounterScreen';
import ColorScreen from './src/screens/ColorScreen';
import Color from './src/components/colors';

const navigator = createStackNavigator(
  {
    Home: HomeScreen,
    First:FirstComponent,
    list:arr,
    ImgDetails:ImgDetails,
    Counter:CounterScreen,
    Colors:ColorScreen,
    Color:Color
  },
  {
    initialRouteName: 'Home',
    defaultNavigationOptions: {
      title: 'App'
    }
  }
);

export default createAppContainer(navigator);
