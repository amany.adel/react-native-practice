import React from 'react';
import {View ,Text ,StyleSheet , Button} from 'react-native';

const Color=({colorName ,onCrease ,onDecrease})=>{

    return (
        <View>
           <Button 
            title={`increase ${colorName}`}
            onPress={()=>onCrease()}/>

           <Button title={`deccrease ${colorName}`}
           onPress={()=>onDecrease()}
           />
               
        </View>
    );
     
}


export default Color;