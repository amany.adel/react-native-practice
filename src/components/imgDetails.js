import React from 'react';
import {View,Text,StyleSheet,Image} from 'react-native';


const ImgDetails=(props)=>{
    return (
    <View>
        <Text>{props.title}</Text>
        <Image source={props.imgs}/>
    </View>
    );
        
};


export default ImgDetails;