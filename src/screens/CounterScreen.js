import React,{useState,useReducer} from 'react';
import {View,Text,StyleSheet,Button} from 'react-native';

const reducer=(state,action)=>{
// state :{init:0}
// action:
return ({state,init:state.init+action.mount});
};
const CounterScreen=()=>{
    const [state,dispitch]=useReducer(reducer,{init:0})
    // const [counter,setCounter]=useState(0);
    return (
        <View>
            <Text>the counter is : {state.init}</Text>
            <Button
                title="increase" 
                onPress={() => dispitch({mount:1})}
            /> 
            <Button
                title="decrease" 
                onPress={() => dispitch({mount:-1})}
            />
        </View>
    );
}


export default CounterScreen;