import React from 'react';
import {Text,View,StyleSheet,FlatList} from 'react-native';

const arrList=()=>{
    const freind=[
        {name:'freind 1',age:15,key:'1'},
        {name:'freind 2',age:19,key:'2'},
        {name:'freind 3',age:19,key:'3'},
        {name:'freind 4',age:15,key:'4'},
        {name:'freind 5',age:13,key:'5'},
        {name:'freind 6',age:11,key:'6'},
        {name:'freind 7',age:18,key:'7'},
        {name:'freind 8',age:17,key:'8'},
        {name:'freind 9',age:16,key:'9'},
        {name:'freind 10',age:15,key:'10'}
    ];
return (
    <FlatList 
    
    data={freind} 
    // keyExtractor={(f) => f.name}

    renderItem={
        ({item})=>{
       return <Text>{item.name +'- Age'+item.age}</Text>
    }}
    // keyExtractor={(item, index) => index.toString()}

    />
    

);

};

const styles=StyleSheet.create({
    marginTest:{
        marginVertical:50
    }
});


export default arrList;