import React,{useState} from 'react';
import {View ,Button,StyleSheet,Text} from 'react-native';
import Color from '../components/colors';

const ColorScreen=()=>{
    const [initRed,setRed]=useState(0);
    const [initBlue,setBlue]=useState(0);
    const [initGreen,setGreen]=useState(0);



return(
    <View>
        <Color 
            colorName="red"
            onCrease={()=>setRed(initRed+10)} 
            onDecrease={()=>setRed(initRed-10)} 
        />
        <Color 
            colorName="green"
            onCrease={()=>setGreen(initGreen+10)} 
            onDecrease={()=>setGreen(initGreen-10)}  
        />
        <Color 
            colorName="blue"
            onCrease={()=>setBlue(initBlue+10)} 
            onDecrease={()=>setBlue(initBlue-10)}  
        /> 
        <View 
            style={
            {width:100 , 
            height:100,
            backgroundColor:`rgb(${initRed},${initGreen},${initBlue})`}}>

        </View>
    </View>
);
}

// const rgbRandom=()=>{

//     const red=Math.floor(Math.random()*256);
//     const green=Math.floor(Math.random()*256);
//     const blue=Math.floor(Math.random()*256);

//     return `rgb(${red},${green},${blue})`;

// }

const styles=StyleSheet.create({

});
export default ColorScreen;