import React from 'react';
import {Text,StyleSheet,View} from 'react-native';
import ImgDetails from '../components/imgDetails';
const FirstComponent=()=>{
    return (
    <View>
        <ImgDetails  title='mountain' imgs={require('../../assets/forest.jpg')}/>
        <ImgDetails  title='img2'     imgs={require('../../assets/beach.jpg')}/>
        <ImgDetails  title='img3'     imgs={require('../../assets/mountain.jpg')}/>
    </View>
    );
};

const styles=StyleSheet.create({
    textStyleFirst:{
        fontSize:30,
        color:'red'
    },
    textStyleSecond:{
        fontSize:20,
        color:'blue'

    }
});


export default FirstComponent;